# Markdown-LaTeX

[Markdown-LaTeX](https://github.com/johannesjh/markdown-latex) is a template and demonstration of how to embed Markdown in LateX documents, as suggested in this [stackoverflow answer](http://tex.stackexchange.com/a/101731).

The resulting PDF output is styled as in stackoverflow, as provided in this [stackoverflow answer](http://tex.stackexchange.com/a/149123).


## Installation

Installation pre-requisites are XeLateX (this typically comes with any LateX distribution of your choice, e.g., MacTeX for Mac OS X) the following fonts.

### Fonts

Follow these [instructions on stackoverflow](http://tex.stackexchange.com/a/149123) to download and install the Source Sans Pro and Source Code Pro fonts in your operating system.


## Usage

To compile the example `markdownlatex.tex` document, simply run `make`, or manually run the following command twice:

	xelatex --shell-escape markdownlatex.tex

